using System;
using System.Collections;
using Gtk;
using Epiphany;

namespace Epiphany {
	public abstract class EphyExtension : GLib.Object, IEphyExtension
	{		
	
		private Hashtable windows;
		
	    public EphyExtension () : base (IntPtr.Zero)
	    {
	    	this.windows = new Hashtable();
	    }
		
		//Provide here any method that could be interseting for extension developpers
		
		//Easily save and retreive data associated to various epiphany windows
		public void SetWindowData (EphyWindow window, object o)
		{
			this.windows[window] = o;
		}
		
		public object GetWindowData (EphyWindow window)
		{
			return this.windows[window];
		}
		
		public object RemoveWindowData (EphyWindow window)
		{
			object o = this.windows[window];
			this.windows.Remove(window);
			return o;
		}
		
		//Methods have to be implemented in plugin
	    public abstract void AttachWindow (EphyWindow window);
		public abstract void DetachWindow (EphyWindow window);
		public abstract void DetachTab (EphyWindow window, EphyTab tab);
	    public abstract void AttachTab (EphyWindow window, EphyTab tab);
    }
}
