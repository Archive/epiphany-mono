dnl Copyright (c) 2003, 2004 Christian Persch
dnl
dnl This program is free software; you can redistribute it and/or modify it
dnl under the terms of the GNU General Public License as published by the
dnl Free Software Foundation; either version 2 of the License, or (at your
dnl option) any later version.
dnl
dnl This program is distributed in the hope that it will be useful, but
dnl WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License along
dnl with this program; if not, write to the Free Software Foundation, Inc.,
dnl 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

AC_INIT([Epiphany MONO],[0.0],[http://bugzilla.gnome.org/enter_bug.cgi?product=epiphany],[epiphany-mono])
API_VERSION=0.0.0.1

AC_PREREQ([2.59])

AC_CONFIG_SRCDIR([loader])
AC_CONFIG_HEADERS([config.h])

AM_INIT_AUTOMAKE([dist-bzip2 no-dist-gzip])

AM_MAINTAINER_MODE
if test "x$enable_maintainer_mode" = "xyes"; then
	AC_DEFINE([MAINTAINER_MODE],[1],[Define to enable 'maintainer-only' behaviour])
	enable_debug=yes
	DEPRECATION_FLAGS="-DG_DISABLE_DEPRECATED -DGDK_DISABLE_DEPRECATED -DGDK_PIXBUF_DISABLE_DEPRECATED -DGCONF_DISABLE_DEPRECATED -DGNOME_VFS_DISABLE_DEPRECATED -DBONOBO_UI_DISABLE_DEPRECATED -DBONOBO_DISABLE_DEPRECATED -DLIBGLADE_DISABLE_DEPRECATED -DPANGO_DISABLE_DEPRECATED -DGTK_DISABLE_DEPRECATED -DGNOME_DISABLE_DEPRECATED"
fi

GNOME_COMMON_INIT

AC_PROG_INTLTOOL([0.29])

AM_DISABLE_STATIC
AC_ENABLE_SHARED([yes])
AC_ENABLE_STATIC([no])

AM_PROG_LIBTOOL

AC_ISC_POSIX
AC_PROG_CC
AM_PROG_CC_STDC
AC_HEADER_STDC
AC_PROG_INSTALL
AC_PROG_MAKE_SET

GNOME_DEBUG_CHECK
GNOME_COMPILE_WARNINGS([error])

GLIB_REQUIRED=2.6.0
GTK_REQUIRED=2.6.0
EPIPHANY_REQUIRED=1.9
EPIPHANY_API=1.10
MONO_REQUIRED=1.1.9
GTK_SHARP_REQUIRED=2.3.92
GAPI_REQUIRED=2.3.92

PKG_CHECK_MODULES([EPIPHANY_DEPENDENCY], [ \
		  glib-2.0 >= $GLIB_REQUIRED \
		  gtk+-2.0 >= $GTK_REQUIRED \
		  gmodule-2.0 \
		  epiphany-$EPIPHANY_API >= $EPIPHANY_REQUIRED])
AC_SUBST([EPIPHANY_DEPENDENCY_CFLAGS])
AC_SUBST([EPIPHANY_DEPENDENCY_LIBS])

EXTENSIONS_DIR="`$PKG_CONFIG --variable=extensionsdir epiphany-$EPIPHANY_API`"
LOADER_DIR="`$PKG_CONFIG --variable=loaderdir epiphany-$EPIPHANY_API`"
AC_DEFINE_UNQUOTED([EXTENSIONS_DIR],["$EXTENSIONS_DIR"],[The extension directory])
AC_DEFINE_UNQUOTED([LOADER_DIR],["$LOADER_DIR"],[The loader directory])
AC_SUBST([LOADER_DIR])
AC_SUBST([EXTENSIONS_DIR])

dnl **********
dnl Mono stuff
dnl **********

PKG_CHECK_MODULES([MONO], [mono >= $MONO_REQUIRED])
AC_SUBST([MONO_CFLAGS])
AC_SUBST([MONO_LIBS])

AC_PATH_PROG([CSC],[mcs],[no])
if test "x$CSC" = "xno"; then
	AC_MSG_ERROR([mcs not found])
fi

AC_PATH_PROG([GACUTIL],[gacutil],[no])
if test "x$GACUTIL" = "xno"; then
	AC_MSG_ERROR([gacutil not found])
fi

PKG_CHECK_MODULES([GAPI],[gapi-2.0 > $GAPI_REQUIRED])
AC_PATH_PROG([GAPI_CODEGEN],[gapi2-codegen],[no])
if test "x$GAPI_CODEGEN" = "xno"; then
	AC_MSG_ERROR([gapi2-codegen not found])
fi

AC_PATH_PROG([GAPI_FIXUP],[gapi2-fixup],[no])
if test "x$GAPI_FIXUP" = "xno"; then
	AC_MSG_ERROR([gapi2-fixup not found])
fi

GACUTIL_FLAGS='/package epiphany-mono /gacdir $(libdir) /root $(DESTDIR)$(libdir)'
AC_SUBST(GACUTIL_FLAGS)

AC_SUBST([CSC])
AC_SUBST([GACUTIL])
AC_SUBST([GAPI_CODEGEN])
AC_SUBST([GAPI_FIXUP])

GAPI_PREFIX="`$PKG_CONFIG --variable=prefix gapi-2.0`"
AC_SUBST([GAPI_PREFIX])

PKG_CHECK_MODULES([GTK_SHARP],[gtk-sharp-2.0 > $GTK_SHARP_REQUIRED])
AC_SUBST([GTK_SHARP_CFLAGS])
AC_SUBST([GTK_SHARP_LIBS])

AC_SUBST([API_VERSION])

dnl *************************************************
dnl Epiphany Source dir stuff (for generating the API)
dnl *************************************************

AC_ARG_WITH([epiphany-source-dir],
    AS_HELP_STRING([--with-epiphany-source-dir@<:@=/path/to/epiphany/source@:>@],
		   [The path to the epiphany source directory]))

if test "x$with_epiphany_source_dir" != "x"; then
   EPHY_SOURCE_DIR=$with_epiphany_source_dir
   if test ! -f "$EPHY_SOURCE_DIR/src/ephy-shell.h" ; then
	AC_MSG_ERROR([$EPHY_SOURCE_DIR did not look like an epiphany source directory])
   fi

   AC_DEFINE_UNQUOTED([EPHY_SOURCE_DIR],["$EPHY_SOURCE_DIR"],[The epiphany source directory])

   AC_PATH_PROG([GAPI2_PARSER],[gapi2-parser],[no])
   if test "x$GAPI2XML" = "xno"; then
	AC_MSG_ERROR([gapi2-parser not found])
   fi

   
   AC_SUBST([GAPI2_PARSER])

   EPHY_INCLUDE_DIR="`$PKG_CONFIG --variable=includedir epiphany-$EPIPHANY_API`"
   AC_SUBST([EPHY_INCLUDE_DIR])
fi
AC_SUBST([EPHY_SOURCE_DIR])
AM_CONDITIONAL([HAVE_EPHY_SOURCE_DIR], [test ! "x$EPHY_SOURCE_DIR" = "x"])

dnl *****************
dnl Add warning flags
dnl *****************

AM_CPPFLAGS="$AM_CPPFLAGS $DEPRECATION_FLAGS"
AM_CFLAGS="$AM_CFLAGS $WARN_CFLAGS"
AC_SUBST([AM_CPPFLAGS])
AC_SUBST([AM_CFLAGS])

dnl ********************************
dnl Internationalisation
dnl ********************************

dnl Translators: new languages must be added to the po/LINGUAS file
ALL_LINGUAS="`cat "$srcdir/po/LINGUAS" | grep -v '^#'`"

AC_SUBST([CONFIG_STATUS_DEPENDENCIES],['$(top_srcdir)/po/LINGUAS'])

GETTEXT_PACKAGE=epiphany-mono-0.0
AC_SUBST([GETTEXT_PACKAGE])
AC_DEFINE_UNQUOTED([GETTEXT_PACKAGE],["$GETTEXT_PACKAGE"], [Gettext package])
AM_GLIB_GNU_GETTEXT
AM_GLIB_DEFINE_LOCALEDIR([LOCALE_DIR])

dnl *******************************
dnl *******************************

AC_CONFIG_FILES([
Makefile
include/Makefile
loader/Makefile
ephy-mono/AssemblyInfo.cs
ephy-mono/Makefile
examples/Makefile
po/Makefile.in
])

AC_OUTPUT

if test ! -f $EXTENSIONS_DIR/extensions-manager-ui.ephy-extension ; then
   echo ""
   echo "=================================================================="
   echo "It looks like the epiphany-extensions package isn't installed."
   echo "This provides a UI for enabling and disabling extensions. Without"
   echo "it you need to enable your mono extensions using gconf."
   echo ""
   echo "In short - install 'epiphany-extensions' :-)"
   echo "=================================================================="
   echo ""
fi
