using System;
using Gtk;
using System.Collections;
using Epiphany;

namespace Test {

   public struct WindowData {
      public ActionGroup group;
      public uint id;
   };

   public class Test : EphyExtension {

      static ActionEntry[] entries = new ActionEntry[] {
         new ActionEntry ("monoaction", null, "Test Mono", null, null, 
                          new EventHandler (OnTestMono))
      };

      static void OnTestMono (object obj, EventArgs args)
      {
         Console.WriteLine ("Mono Extension: Loading www.go-mono.com");
         EphyShell.Default.Session.ActiveWindow.ActiveEmbed.LoadUrl ("http://www.go-mono.com");
      }
		
      public override void AttachWindow (EphyWindow window)
      {
         ActionGroup group = new ActionGroup ("MonoActions");
         group.Add (entries);

         UIManager uim = (Gtk.UIManager) window.UiManager;
         uim.InsertActionGroup (group, 0);
         uint id = uim.NewMergeId();
         uim.AddUi (id, "/menubar/ToolsMenu", "monoaction", "monoaction",
                    UIManagerItemType.Menuitem, false);

         WindowData data = new WindowData();
         data.group = group;
         data.id = id;
         SetWindowData(window, data);

         System.Console.WriteLine ("Mono Extension: Attach window");
      }

      public override void DetachWindow (EphyWindow window)
      {
         WindowData data = (WindowData) RemoveWindowData(window);

         UIManager uim = (Gtk.UIManager) window.UiManager;
         uim.RemoveUi (data.id);
         uim.RemoveActionGroup (data.group);

         System.Console.WriteLine ("Mono Extension: Detach window");
      }

      private void OnGeLocation(object sender, GeLocationArgs args)
      {
         System.Console.WriteLine( "Mono Extension: {0}", args.Location );
      }

      public override void DetachTab (EphyWindow window, EphyTab tab)
      {
         System.Console.WriteLine ("Mono Extension: Detach tab");
         EphyEmbed embed = tab.Embed;
         embed.GeLocation -= this.OnGeLocation;
      }

      public override void AttachTab (EphyWindow window, EphyTab tab)
      {
         System.Console.WriteLine ("Mono Extension: Attach tab {0}", tab.DocumentType );
         EphyEmbed embed = tab.Embed;
         embed.GeLocation += this.OnGeLocation;
      }
   }
}
