using System;
using Gtk;
using System.Collections;
using Epiphany;

namespace EphyRssExtension {
   public class RssExtension : EphyExtension {		
      private Hashtable feeds;

      //TODO: Provide a LOG-like routine ?
      public RssExtension () {
         Console.WriteLine ("New EphyRss Extension");
			
         this.feeds = new Hashtable ();
      }

      public override void AttachWindow (EphyWindow window)
      {		    
         ActionGroup group = new ActionGroup ("EphyRssExtensionActions");

         ActionEntry[] entries = new ActionEntry[] {
            new ActionEntry ("RssInfo", null, "News Feeds Subscription", null,
                             "Subscribe to this website's news feed in your favorite news reader",
                             new EventHandler (OnMenuAction))
         };

         group.Add (entries);

         UIManager uim = window.UiManager;
         uim.InsertActionGroup (group, 0);
         uint id = uim.NewMergeId();
		    
         uim.AddUi (id, "/menubar/ToolsMenu", "RssInfoSep", null, UIManagerItemType.Separator, false);
         uim.AddUi (id, "/menubar/ToolsMenu", "RssInfo", "RssInfo", UIManagerItemType.Menuitem, false);
         uim.AddUi (id, "/menubar/ToolsMenu",  "RssInfoSep2", null, UIManagerItemType.Separator, false);
			
         //TODO: Connecting to a property ?
         // Apparently GLib.Object.AddNotification / RemoveNotification - crispin
         //g_signal_connect_after (window, "notify::active-tab",
         //	G_CALLBACK (ephy_rss_sync_active_tab), NULL);
			
         WindowData data = new WindowData ();
         data.Group = group;
         data.Id = id;
         data.Action = group.GetAction ("RssInfo");
         CreateStatusIcon (window, ref data);
						
         data.Action.Sensitive = true;
						
         SetWindowData (window, data);
      }

      private void CreateStatusIcon (EphyWindow window, ref WindowData data)
      {		
         Frame frame = new Frame ();
         EventBox evbox = new EventBox ();
			
         frame.Add (evbox);
         evbox.Show ();
			
         Image img = new Image ("stock_hyperlink-toolbar", IconSize.Menu);
         evbox.Add (img);
         img.Show ();
			
         Tooltips tip = new Tooltips ();
         tip.SetTip (evbox, "Subscribe to site's news feed", "");
			
         window.Statusbar.Add (frame);

         evbox.ButtonPressEvent += new ButtonPressEventHandler (OnStatusClicked);
			
         frame.Show ();
			
         data.Evbox = evbox;
         data.Frame = frame;
      }
		
      public override void DetachWindow (EphyWindow window)
      {
         WindowData data = (WindowData) GetWindowData (window);
			
         UIManager uim = window.UiManager as UIManager;
         uim.RemoveUi (data.Id);
         uim.RemoveActionGroup (data.Group);

         (window.Statusbar as EphyStatusbar).Remove (data.Frame);
         data.Evbox.ButtonPressEvent -= new ButtonPressEventHandler (OnStatusClicked);
      }

      public override void DetachTab (EphyWindow window, EphyTab tab)
      {
         System.Console.WriteLine ("Mono Extension: Detach tab");
		    
         feeds.Remove(tab.Embed);
         tab.Embed.GeFeedLink -= OnFeedReceived;
         tab.Embed.GeContentChange -= OnContentChange;
      }

      public override void AttachTab (EphyWindow window, EphyTab tab)
      {
         System.Console.WriteLine ("Mono Extension: Attach tab: {0}", tab.DocumentType );

         feeds[tab.Embed] = new ArrayList();
         tab.Embed.GeFeedLink += OnFeedReceived;
         tab.Embed.GeContentChange += new GeContentChangeHandler (OnContentChange);
		    
      }
		
      private void OnFeedReceived (object o, GeFeedLinkArgs args)
      {
         Console.WriteLine ("Feed {0}: '{1}' @ {2}", args.Type, args.Title, args.Address);
         IList l = feeds[o] as IList;
			
         Feed f = new Feed();
         f.Title = args.Title;
         f.Type = args.Type;
         f.Address = args.Address;
			
         l.Add(f);
			
         //UpdateAvailability ();
      }
		
      private void UpdateAvailability (EphyWindow window)
      {
         Console.WriteLine ("Should (de-)activate menu item/statusbar");
         bool show = (feeds[window.ActiveEmbed] as IList).Count > 0;

         WindowData data = (WindowData) GetWindowData (window);
			
         data.Action.Sensitive = show;
         data.Frame.Visible = show;
      }
		
      private void OnMenuAction (object obj, EventArgs args)
      {
         Console.WriteLine ("Should have opened a dialog here");
      }
		
      private void OnStatusClicked  (object obj, ButtonPressEventArgs args)
      {
         Console.WriteLine ("Should have opened a dialog here");
      }
		
      private void OnContentChange  (object obj, GeContentChangeArgs args)
      {
         (feeds[obj] as IList).Clear ();
			
         //UpdateAvailability ();
      }
	
	private class WindowEventHandler {
		private EphyWindow window;
		
		public WindowEventHandler (EphyWindow window)
		{
			this.window = window;
		}
		
		
	}
	
      private struct Feed {
         public string Title;
         public string Type;
         public string Address;
      }
		
      private struct WindowData {
         public ActionGroup 	Group;
         public Action		Action;
         public uint 		Id;
         public EventBox 	Evbox;
         public Frame 		Frame;
      }
   }
}
