/*
 *  Copyright (C) 2003 Marco Pesenti Gritti
 *  Copyright (C) 2003 Christian Persch
 *  Copyright (C) 2004, 2005 Adam Hooper
 *  Copyright (C) 2005 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <epiphany/ephy-extension.h>
#include <epiphany/ephy-window.h>
#include <epiphany/ephy-tab.h>

#include "ephy-file-helpers.h"
#include "ephy-debug.h"

#include "ephy-mono-extension.h"
#include "ephy-mono-helpers.h"
#include "ephy-mono-loader.h"

#include <mono/metadata/tokentype.h>
#include <string.h>

#define EPHY_MONO_EXTENSION_GET_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE ((object),\
                                                 EPHY_TYPE_MONO_EXTENSION, EphyMonoExtensionPrivate))
static GObjectClass *parent_class = NULL;
static GType type = 0;


struct _EphyMonoExtensionPrivate
{
	char * filename;

	GHashTable *windows;
	GHashTable *tabs;

	MonoObject *object;
	MonoClass *klass;

	MonoMethod *attach_window_func;
	MonoMethod *detach_window_func;
	MonoMethod *attach_tab_func;
	MonoMethod *detach_tab_func;

	MonoMethod *object_wrap_func;
};

enum
{
	PROP_0,
	PROP_FILENAME
};

/* Wrap an object, this is all a bit odd, as MonoObject's have to
 * be on the heap (not the stack) */
static EphyMonoHandle *
ephy_mono_extension_wrap_object (EphyMonoExtension *ext, gpointer object)
{
	gpointer args[1];
	
	args[0] = &object;
	
	return ephy_mono_helper_mono_invoke  (NULL, ext->priv->object_wrap_func, args);
}


static void
call_mono_func (EphyMonoExtension *ext,
		MonoMethod *method,
		EphyWindow *window,
		EphyTab *tab)
{
	EphyMonoHandle *ret, *handle;
	gpointer* args;

	g_return_if_fail (EPHY_IS_WINDOW (window));
	g_return_if_fail (!tab || EPHY_IS_TAB (tab));

	if (!method) return;

	args = g_new0(gpointer, tab ? 2 : 1);

	handle = g_hash_table_lookup (ext->priv->windows, window);
	g_assert (handle != NULL);

	args[0] = handle->object;

	if (tab)
	{
		handle = g_hash_table_lookup (ext->priv->tabs, tab);
		g_assert (handle != NULL);

		args[1] = handle->object;
	}

	ret = ephy_mono_helper_mono_invoke (ext->priv->object, method, args);
	ephy_mono_handle_free (ret);
	g_free (args);
}


static void
impl_attach_tab (EphyExtension *extension,
		 EphyWindow *window,
		 EphyTab *tab)
{
	EphyMonoExtension *ext = (EphyMonoExtension *) extension;

	LOG ("EphyMonoExtension::AttachTab (%p, %p, %p)", ext, window, tab);

	g_assert (g_hash_table_lookup (ext->priv->tabs, tab) == NULL);
	g_hash_table_insert (ext->priv->tabs, tab,
			     ephy_mono_extension_wrap_object (ext, tab));

	call_mono_func (ext, ext->priv->attach_tab_func, window, tab);
}

static void
impl_detach_tab (EphyExtension *extension,
		 EphyWindow *window,
		 EphyTab *tab)
{
	EphyMonoExtension *ext = (EphyMonoExtension *) extension;

	LOG ("EphyMonoExtension::DetachTab (%p, %p, %p)", ext, window, tab);

	call_mono_func (ext, ext->priv->detach_tab_func, window, tab);

	g_hash_table_remove (ext->priv->windows, tab);
}

static void
impl_attach_window (EphyExtension *extension,
		    EphyWindow *window)
{
	EphyMonoExtension *ext = (EphyMonoExtension *) extension;

	LOG ("EphyMonoExtension::AttachWindow (%p, %p)", ext, window);

	g_assert (g_hash_table_lookup (ext->priv->windows, window) == NULL);
	g_hash_table_insert (ext->priv->windows, window,
			     ephy_mono_extension_wrap_object (ext, window));

	call_mono_func (ext, ext->priv->attach_window_func, window, NULL);
}

static void
impl_detach_window (EphyExtension *extension,
		    EphyWindow *window)
{
	EphyMonoExtension *ext = (EphyMonoExtension *) extension;

	LOG ("EphyMonoExtension::DetachWindow (%p, %p)", ext, window);

	call_mono_func (ext, ext->priv->detach_window_func, window, NULL);

	g_hash_table_remove (ext->priv->windows, window);
}

static void
ephy_mono_extension_iface_init (EphyExtensionIface *iface)
{
	iface->attach_tab = impl_attach_tab;
	iface->detach_tab = impl_detach_tab;
	iface->attach_window = impl_attach_window;
	iface->detach_window = impl_detach_window;
}


static MonoClass*
load_assembly (const char *filename)
{
	MonoAssembly *assembly;
	MonoImage *image;
	MonoClass *klass;
	int i, total;

	LOG ("loading %s", filename);

	assembly = mono_domain_assembly_open (ephy_mono_get_domain(), filename);
	if (!assembly)
	{
		g_warning ("Couldn't load assembly for %s", filename);
		return NULL;
	}

	image = mono_assembly_get_image (assembly);
	
	total = mono_image_get_table_rows (image, MONO_TABLE_TYPEDEF);
	LOG( "Found %d classes in assembly", total);

	for (i = 1; i <= total; i++)
	{
		MonoClass *pklass;
		
		klass = mono_class_get (image, MONO_TOKEN_TYPE_DEF | i);
		pklass = mono_class_get_parent (klass);
		if (!pklass) continue;

		
		if (strcmp("EphyExtension", mono_class_get_name (pklass)) == 0)
		{
			LOG( "%s extends EphyExtension",
			     mono_class_get_name (klass));
			return klass;
		}
	}

	g_warning ("Couldn't find class derived from 'EphyExtension' in assembly at %s",
		   filename);
	return klass;
}

static void
ephy_mono_module_load (EphyMonoExtension *ext)
{
	gchar *files[4], *file = NULL;
	int i;

	LOG ("Loading %s", ext->priv->filename);

        files[0] = g_strdup (ext->priv->filename);
        files[1] = g_build_filename (ephy_dot_dir(), "extensions", ext->priv->filename, NULL);
        files[2] = g_build_filename (EXTENSIONS_DIR, ext->priv->filename, NULL);
	files[3] = NULL;

	for (i = 0; files[i]; i++)
	{
		if (file == NULL && g_file_test (files[i], G_FILE_TEST_EXISTS)) 
		{
			file = files[i];
		}
		else
		{
			/* free unused string */
			g_free (files[i]);
		}
	}

	if (! file)
	{
		g_warning ("%s not found", ext->priv->filename);
		return;
	}
        
	ext->priv->klass = load_assembly (file);
}

static GObject *
ephy_mono_extension_constructor (GType type,
				 guint n_construct_properties,
				 GObjectConstructParam *construct_params)
{
	GObject *object;
	EphyMonoExtension *ext;
	MonoAssembly *glib_assembly;

	object = parent_class->constructor (type, n_construct_properties,
					    construct_params);

	ext = EPHY_MONO_EXTENSION (object);

	ephy_mono_module_load (ext);

	if (!ext->priv->klass)
	{
		g_warning ("Failed to load class from mono assembly");
		return object;
	}
	
	ext->priv->object = mono_object_new (ephy_mono_get_domain(), ext->priv->klass);
	mono_runtime_object_init (ext->priv->object);

	/* Lookup the methods */
	ext->priv->attach_window_func = 
		ephy_mono_helper_lookup_method (ext->priv->klass,
						":AttachWindow(Epiphany.EphyWindow)",
						TRUE);
	ext->priv->detach_window_func =
		ephy_mono_helper_lookup_method (ext->priv->klass, 
						":DetachWindow(Epiphany.EphyWindow)",
						TRUE);
	ext->priv->attach_tab_func =
		ephy_mono_helper_lookup_method (ext->priv->klass,
						":AttachTab(Epiphany.EphyWindow,Epiphany.EphyTab)",
						TRUE);
	ext->priv->detach_tab_func = 
		ephy_mono_helper_lookup_method (ext->priv->klass,
						":DetachTab(Epiphany.EphyWindow,Epiphany.EphyTab)",
						TRUE);

	/* Get the function that is used to wrap the objects */
	glib_assembly = mono_assembly_load_with_partial_name ("glib-sharp", NULL);		
	g_assert (glib_assembly);

	ext->priv->object_wrap_func = 
		ephy_mono_helper_lookup_function (glib_assembly,
						  "GLib.Object:GetObject(intptr)", 
						  TRUE);

	LOG ("Constructed object mono %p", ext->priv->object);

	return object;
}

static void 
ephy_mono_extension_init (EphyMonoExtension *ext)
{
	LOG ("EphyMonoExtension initialising");

	ext->priv = EPHY_MONO_EXTENSION_GET_PRIVATE (ext);

	ext->priv->windows = g_hash_table_new_full (NULL, NULL, NULL, 
						    (GDestroyNotify)ephy_mono_handle_free);
	ext->priv->tabs = g_hash_table_new_full (NULL, NULL, NULL,
						 (GDestroyNotify)ephy_mono_handle_free);
}

static void
ephy_mono_extension_finalize (GObject *obj)
{
	EphyMonoExtension *ext = EPHY_MONO_EXTENSION (obj);

	LOG ("EphyMonoExtension finalizing");
	
	g_free (ext->priv->filename);
	g_hash_table_destroy (ext->priv->windows);
	g_hash_table_destroy (ext->priv->tabs);

	/* Free all the methods */
	if (ext->priv->attach_window_func)
	{
		mono_free_method (ext->priv->attach_window_func);
	}

	if (ext->priv->detach_window_func)
	{
		mono_free_method (ext->priv->detach_window_func);
	}

	if (ext->priv->attach_tab_func)
	{
		mono_free_method (ext->priv->attach_tab_func);
	}

	if (ext->priv->detach_tab_func)
	{
		mono_free_method (ext->priv->detach_tab_func);
	}

	if (ext->priv->object_wrap_func)
	{
		mono_free_method (ext->priv->detach_tab_func);
	}

	/* TODO What to do with these */
	ext->priv->object = NULL;
	ext->priv->klass = NULL;

	G_OBJECT_CLASS (parent_class)->finalize (obj);
}

static void
ephy_mono_extension_get_property (GObject *object,
				    guint prop_id,
				    GValue *value,
				    GParamSpec *pspec)
{
	/* no readable properties */
	g_return_if_reached ();
}

static void
ephy_mono_extension_set_property (GObject *object,
				    guint prop_id,
				    const GValue *value,
				    GParamSpec *pspec)
{
	EphyMonoExtension *ext = EPHY_MONO_EXTENSION (object);

	switch (prop_id)
	{
        case PROP_FILENAME:
		ext->priv->filename = g_value_dup_string (value);
		break;
	default:
		g_return_if_reached ();
	}
}


static void
ephy_mono_extension_class_init (EphyMonoExtensionClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = ephy_mono_extension_finalize;
	object_class->constructor = ephy_mono_extension_constructor;
	object_class->get_property = ephy_mono_extension_get_property;
	object_class->set_property = ephy_mono_extension_set_property;

	g_object_class_install_property
		  (object_class,
		   PROP_FILENAME,
		   g_param_spec_string ("filename",
					"Filename",
					"Filename",
					NULL,
					G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));


	g_type_class_add_private (object_class, sizeof (EphyMonoExtensionPrivate));
}

GType
ephy_mono_extension_get_type (void)
{
	return type;
}

GType
ephy_mono_extension_register_type (GTypeModule *module)
{
	static const GTypeInfo our_info =
	{
		sizeof (EphyMonoExtensionClass),
		NULL, /* base_init */
		NULL, /* base_finalize */
		(GClassInitFunc) ephy_mono_extension_class_init,
		NULL,
		NULL, /* class_data */
		sizeof (EphyMonoExtension),
		0, /* n_preallocs */
		(GInstanceInitFunc) ephy_mono_extension_init
	};

	static const GInterfaceInfo extension_info =
	{
		(GInterfaceInitFunc) ephy_mono_extension_iface_init,
		NULL,
		NULL
	};

	type = g_type_module_register_type (module,
					    G_TYPE_OBJECT,
					    "EphyMonoExtension",
					    &our_info, 0);

	g_type_module_add_interface (module,
				     type,
				     EPHY_TYPE_EXTENSION,
				     &extension_info);

	return type;
}
