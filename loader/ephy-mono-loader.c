/*
 *  Copyright (C) 2003 Marco Pesenti Gritti
 *  Copyright (C) 2003, 2004 Christian Persch
 *  Copyright (C) 2005 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include "config.h"

#include <mono/jit/jit.h>
#include <mono/metadata/debug-helpers.h>
#include <mono/metadata/threads.h>
#include <mono/metadata/assembly.h>

#include <epiphany/ephy-loader.h>

#include "ephy-mono-loader.h"
#include "ephy-mono-extension.h"
#include "ephy-mono-helpers.h"

#include "ephy-debug.h"
#include <string.h>

#define GROUP	"Mono"

static GObjectClass *parent_class = NULL;

static GType type = 0;

MonoDomain *
ephy_mono_get_domain (void)
{
	static MonoDomain *domain = NULL;
	
	if (!domain)
	{
		LOG ("Initializing mono");
		domain = mono_jit_init("epiphany-mono");
		LOG ("Done Initializing mono");
	}

	return domain;
}

static GObject *
impl_get_object (EphyLoader *eloader,
		 GKeyFile *keyfile)
{
	const char *library;
        GObject *object;

	library = g_key_file_get_string (keyfile, GROUP, "Library", NULL);
	if (library == NULL)
	{
		g_warning ("NULL library name!");
		return NULL;
	}

	object = g_object_new (EPHY_TYPE_MONO_EXTENSION,
			       "filename", library,
			       NULL);

	/* we own one ref */
	return g_object_ref (object);
}

static void
impl_release_object (EphyLoader *eloader,
		     GObject *object)
{
	g_return_if_fail (object != NULL);

	g_object_unref (object);
}

static void
ephy_mono_loader_iface_init (EphyLoaderIface *iface)
{
	iface->type = "mono";
	iface->get_object = impl_get_object;
	iface->release_object = impl_release_object;
}

static void
ephy_mono_loader_init (EphyMonoLoader *loader)
{
	MonoMethod *func;
	MonoAssembly *assembly;

	LOG ("EphyMonoLoader initialising");

	/* Initialize Mono engine */
	mono_thread_attach (ephy_mono_get_domain ());

	/* Initialize the Type system in the epiphany assembly */
	assembly = mono_assembly_load_with_partial_name ("epiphany-sharp", NULL);

        if (assembly)
	{
		func = ephy_mono_helper_lookup_function (assembly,
							 "GtkSharp.EpiphanySharp.ObjectManager:Initialize()",
							 TRUE);
		ephy_mono_helper_mono_invoke  (NULL, func, NULL);
		mono_free_method (func);
	}

	LOG ("Done initializing");
}

static void
ephy_mono_loader_finalize (GObject *object)
{
	LOG ("EphyMonoLoader finalising");

	parent_class->finalize (object);
}

static void
ephy_mono_loader_class_init (EphyMonoLoaderClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = ephy_mono_loader_finalize;
}

GType
ephy_mono_loader_get_type (void)
{
	return type;
}

GType
ephy_mono_loader_register_type (GTypeModule *module)
{
	static const GTypeInfo our_info =
	{
		sizeof (EphyMonoLoaderClass),
		NULL, /* base_init */
		NULL, /* base_finalize */
		(GClassInitFunc) ephy_mono_loader_class_init,
		NULL,
		NULL, /* class_data */
		sizeof (EphyMonoLoader),
		0, /* n_preallocs */
		(GInstanceInitFunc) ephy_mono_loader_init
	};
	static const GInterfaceInfo loader_info =
	{
		(GInterfaceInitFunc) ephy_mono_loader_iface_init,
		NULL,
		NULL
	};

	type = g_type_module_register_type (module,
					    G_TYPE_OBJECT,
					    "EphyMonoLoader",
					    &our_info, 0);

	g_type_module_add_interface (module,
				     type,
				     EPHY_TYPE_LOADER,
				     &loader_info);

	return type;
}
