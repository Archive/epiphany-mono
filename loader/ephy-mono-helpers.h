/*
 *  Copyright (C) 2004 Dave Camp
 *  Copyright (C) 2005 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * Originally from nautilus-mono
 */

#ifndef MONO_HELPERS_H
#define MONO_HELPERS_H

#include <glib-object.h>
#include <mono/jit/jit.h>

/* Some helpers for dealing with the fact that all mono objects
 * must be on the heap */
typedef struct {
	MonoObject *object;
} EphyMonoHandle;

EphyMonoHandle* ephy_mono_handle_new (void);
void            ephy_mono_handle_free (EphyMonoHandle *handle);


MonoMethod * ephy_mono_helper_lookup_method (MonoClass *klass,
                                             const char *method,
                                             gboolean use_namespace);

MonoMethod * ephy_mono_helper_lookup_function (MonoAssembly *assembly,
                                               const char *method,
                                               gboolean use_namespace);



EphyMonoHandle * ephy_mono_helper_mono_invoke (MonoObject *object,
                                               MonoMethod *method, 
                                               void **params);

#endif
