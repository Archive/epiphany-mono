/*
 *  Copyright (C) 2004 Dave Camp
 *  Copyright (C) 2005 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * Originally from nautilus-mono
 */

#include "config.h"

#include "ephy-mono-helpers.h"

#include <glib.h>
#include <glib-object.h>

#include <mono/jit/jit.h>
#include <mono/metadata/debug-helpers.h>
#include <mono/metadata/threads.h>
#include <mono/metadata/assembly.h>

EphyMonoHandle *
ephy_mono_handle_new (void)
{
	return g_new0 (EphyMonoHandle, 1);
}


void
ephy_mono_handle_free (EphyMonoHandle *handle)
{
	if (handle) {
		handle->object = NULL;
		g_free (handle);
	}
}


MonoMethod *
ephy_mono_helper_lookup_method (MonoClass *klass,
				const char *name,
				gboolean use_namespace)
{
	MonoMethodDesc *desc;
	MonoMethod *ret;

	desc = mono_method_desc_new (name, use_namespace);
	if (!desc)
	{
		g_warning ("couldn't parse method desc %s", name);
		return NULL;
	}
	
	ret = mono_method_desc_search_in_class (desc, klass);

	mono_method_desc_free (desc);

	return ret;
}

MonoMethod *
ephy_mono_helper_lookup_function (MonoAssembly *assembly, 
				  const char *name,
				  gboolean use_namespace)
{
	MonoMethodDesc *desc;
	MonoMethod *ret;
	MonoImage *image;

	desc = mono_method_desc_new (name, use_namespace);
	if (!desc)
	{
		g_warning ("couldn't parse method desc %s", name);
		return NULL;
	}

	image = mono_assembly_get_image (assembly);

	ret = mono_method_desc_search_in_image (desc, image);

	mono_method_desc_free (desc);

	return ret;
}

EphyMonoHandle *
ephy_mono_helper_mono_invoke (MonoObject *object,
			      MonoMethod *method, 
			      void **params) 
{
	EphyMonoHandle *ret;
	EphyMonoHandle *exception;

	ret = ephy_mono_handle_new ();
	exception = ephy_mono_handle_new ();

	ret->object = mono_runtime_invoke (method, 
					   object, 
					   params,
					   &exception->object);

	if (exception->object) 
	{
		mono_print_unhandled_exception (exception->object);

		ephy_mono_handle_free (exception);
		ephy_mono_handle_free (ret);
		return NULL;
	}

	ephy_mono_handle_free (exception);
	return ret;
}
